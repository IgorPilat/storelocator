<?php

namespace Elogic\StoreLocator\Controller\Adminhtml\StoreLocator;

use Elogic\StoreLocator\Api\StoreLocatorRepositoryInterface;
use Elogic\StoreLocator\Model\StoreLocator;
use Elogic\StoreLocator\Model\StoreLocatorFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;

class Save extends Action implements HttpPostActionInterface
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Elogic_StoreLocator::edit';

    /**
     * @var StoreLocatorFactory
     */
    private $storeFactory;
    /**
     * @var StoreLocatorRepositoryInterface
     */
    private $storeLocatorRepository;
    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * Save constructor.
     * @param Context $context
     * @param StoreLocatorRepositoryInterface $storeLocatorRepository
     * @param StoreLocatorFactory $storeFactory
     * @param DataPersistorInterface $dataPersistor
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Context $context,
        StoreLocatorRepositoryInterface $storeLocatorRepository,
        StoreLocatorFactory $storeFactory,
        DataPersistorInterface $dataPersistor,
        StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);
        $this->dataPersistor = $dataPersistor;
        $this->storeFactory = $storeFactory;
        $this->storeLocatorRepository = $storeLocatorRepository;
        $this->storeManager = $storeManager;
    }

    public function execute()
    {
        $redirect = $this->resultRedirectFactory->create();

        $data = $this->getRequest()->getPostValue();

        if ($data) {
            if (empty($data['store_id'])) {
                $data['store_id'] = null;
            }

            /** @var StoreLocator $store */
            $store = $this->storeFactory->create();

            $id = $this->getRequest()->getParam('store_id');
            if ($id) {
                try {
                    $store = $this->storeLocatorRepository->getById($id);
                } catch (LocalizedException $e) {
                    $this->messageManager->addErrorMessage(__('This store no longer exists.'));
                    return $redirect->setPath('*/*/');
                }
            }

            if (isset($data['image']) && is_array($data['image'])) {
                $media_url = $this->storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
                $data['image'] = $data['image'][0]['name'];
            } else {
                $data['image'] = null;
            }

            $store->setData($data);

            try {
                $this->storeLocatorRepository->save($store);
                $this->messageManager->addSuccessMessage(__('You saved the store.'));
                return $redirect->setPath("storelocator/storelocator/index", ['store_id' => $store->getId()]);
            } catch (LocalizedException $e) {
                $this->messageManager->addExceptionMessage($e->getPrevious() ?: $e);
            } catch (\Throwable $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the page.'));
            }

            $this->dataPersistor->set('store_locator', $data);

            return $redirect->setPath("storelocator/storelocator/index", ['store_id' => $this->getRequest()->getParam('store_id')]);
        }
        return $redirect->setPath('*/*/');
    }
}
