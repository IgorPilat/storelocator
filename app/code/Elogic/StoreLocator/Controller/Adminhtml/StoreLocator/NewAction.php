<?php

namespace Elogic\StoreLocator\Controller\Adminhtml\StoreLocator;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;

class NewAction extends Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Elogic_StoreLocator::edit';

    /**
     * @var \Magento\Framework\Controller\Result\ForwardFactory
     */
    protected $forwardFactory;


    /**
     * Index constructor.
     * @param Context $context
     * @param \Magento\Framework\Controller\Result\ForwardFactory $forwardFactory
     */
    public function __construct(
        Context $context,
        \Magento\Framework\Controller\Result\ForwardFactory $forwardFactory
    ) {
        parent::__construct($context);
        $this->forwardFactory = $forwardFactory;
    }

    public function execute()
    {
        $resultForward = $this->forwardFactory->create();

        return  $resultForward->forward('edit');
    }
}
