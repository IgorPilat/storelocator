<?php

namespace Elogic\StoreLocator\Observer;

use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Message\ManagerInterface;
use Elogic\StoreLocator\Helper\LatLongCoordinates;
use Elogic\StoreLocator\Helper\UrlKey;


class StorelocatorSaveBeforeObserver implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var LatLongCoordinates
     */
    private $latLongHelper;
    /**
     * @var ManagerInterface
     */
    private $messageManager;
    /**
     * @var UrlKey
     */
    private $urlKeyHelper;

    /**
     * StoreSaveBeforeObserver constructor.
     * @param LatLongCoordinates $latLongHelper
     * @param UrlKey $urlKeyHelper
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        LatLongCoordinates $latLongHelper,
        UrlKey $urlKeyHelper,
        ManagerInterface $messageManager
    ) {
        $this->latLongHelper = $latLongHelper;
        $this->messageManager = $messageManager;
        $this->urlKeyHelper = $urlKeyHelper;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var \Elogic\StoreLocator\Api\Data\StoreLocatorInterface $store */
        $store = $observer->getStorelocator();

        if ($store->getLatitude() == null || $store->getLongitude() == null) {
            $address =  $store->getAddress();
            try {
                $coordinates = $this->latLongHelper->getLatLong($address);
                $store->setLatitude($coordinates['latitude']);
                $store->setLongitude($coordinates['longitude']);
            } catch (CouldNotSaveException $exception) {
                $this->messageManager->addNoticeMessage($exception->getMessage());
            }

        }

        if (strlen($store->getUrlKey()) == 0) {
            try {
                $urlKey = $this->urlKeyHelper->getStoreUrlKey($store->getStoreName());
                $store->setUrlKey($urlKey);
            } catch (CouldNotSaveException $exception) {
                $this->messageManager->addNoticeMessage($exception->getMessage());
            }
        }
    }
}
