<?php

namespace Elogic\StoreLocator\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Elogic\StoreLocator\Api\Data\StoreLocatorInterface;

interface StoreLocatorRepositoryInterface
{
    /**
     * Save store.
     *
     * @param \Elogic\StoreLocator\Api\Data\StoreLocatorInterface $store
     * @return \Elogic\StoreLocator\Api\Data\StoreLocatorInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Elogic\StoreLocator\Api\Data\StoreLocatorInterface $store);

    /**
     * Retrieve store.
     *
     * @param int $storeId
     * @return \Elogic\StoreLocator\Api\Data\StoreLocatorInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($storeId);

    /**
     * Retrieve stores matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Elogic\StoreLocator\Api\Data\StoreLocatorSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete store.
     *
     * @param \Elogic\StoreLocator\Api\Data\StoreLocatorInterface $store
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Elogic\StoreLocator\Api\Data\StoreLocatorInterface $store);

    /**
     * Delete store by ID.
     *
     * @param int $storeId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($storeId);
}
