<?php

namespace Elogic\StoreLocator\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface StoreLocatorSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get stores list.
     *
     * @return \Elogic\StoreLocator\Api\Data\StoreLocatorInterface[]
     */
    public function getItems();

    /**
     * Set stores list.
     *
     * @param \Elogic\StoreLocator\Api\Data\StoreLocatorInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
