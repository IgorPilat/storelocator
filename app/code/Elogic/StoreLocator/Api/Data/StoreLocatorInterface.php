<?php

namespace Elogic\StoreLocator\Api\Data;

interface StoreLocatorInterface
{
    const STORE_ID = "store_id";
    const STORE_NAME = "store_name";
    const DESCRIPTION = "store_description";
    const IMAGE = "image";
    const CITY = "city";
    const COUNTRY = "country";
    const ADDRESS = "store_address";
    const WORK_TIME = "store_work_time";
    const LONGITUDE = "store_longitude";
    const LATITUDE = "store_latitude";
    const URL_KEY = "store_url";

    /**
     * Get ID
     *
     * @return int/null
     */
    public function getId();

    /**
     * Get store name
     *
     * @return string
     */
    public function getStoreName();

    /**
     * Get description
     *
     * @return string|null
     */
    public function getDescription();

    /**
     * Get image
     *
     * @return string|null
     */
    public function getImage();

    /**
     * Get city
     *
     * @return string|null
     */
    public function getCity();

    /**
     * Get country
     *
     * @return string|null
     */
    public function getCountry();

    /**
     * Get address
     *
     * @return string|null
     */
    public function getAddress();

    /**
     * Get work time
     *
     * @return string|null
     */
    public function getWorkTime();

    /**
     * Get longitude
     *
     * @return float|null
     */
    public function getLongitude();

    /**
     * Get latitude
     *
     * @return float|null
     */
    public function getLatitude();

    /**
     * Get url key
     *
     * @return string
     */
    public function getUrlKey();

    /**
     * Set ID
     *
     * @param int $id
     * @return \Elogic\StoreLocator\Api\Data\StoreLocatorInterface
     */
    public function setId($id);

    /**
     * Set store name
     *
     * @param string $name
     * @return \Elogic\StoreLocator\Api\Data\StoreLocatorInterface
     */
    public function setStoreName($name);

    /**
     * Set description
     *
     * @param string $description
     * @return \Elogic\StoreLocator\Api\Data\StoreLocatorInterface
     */
    public function setDescription($description);

    /**
     * Set image
     *
     * @param string $image
     * @return \Elogic\StoreLocator\Api\Data\StoreLocatorInterface
     */
    public function setImage($image);

    /**
     * Set city
     *
     * @param string $city
     * @return \Elogic\StoreLocator\Api\Data\StoreLocatorInterface
     */
    public function setCity($city);

    /**
     * Set country
     *
     * @param string $country
     * @return \Elogic\StoreLocator\Api\Data\StoreLocatorInterface
     */
    public function setCountry($country);

    /**
     * Set address
     *
     * @param string $address
     * @return \Elogic\StoreLocator\Api\Data\StoreLocatorInterface
     */
    public function setAddress($address);

    /**
     * Set work time
     *
     * @param string $workTime
     * @return \Elogic\StoreLocator\Api\Data\StoreLocatorInterface
     */
    public function setWorkTime($workTime);

    /**
     * Set longitude
     *
     * @param float $longitude
     * @return \Elogic\StoreLocator\Api\Data\StoreLocatorInterface
     */
    public function setLongitude($longitude);

    /**
     * Set latitude
     *
     * @param float $latitude
     * @return \Elogic\StoreLocator\Api\Data\StoreLocatorInterface
     */
    public function setLatitude($latitude);

    /**
     * Set store url key
     *
     * @param string $urlKey
     * @return \Elogic\StoreLocator\Api\Data\StoreLocatorInterface
     */
    public function setUrlKey($urlKey);
}
