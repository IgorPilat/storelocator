<?php

declare(strict_types=1);

namespace Elogic\StoreLocator\Model;

use Elogic\StoreLocator\Api\Data\StoreLocatorSearchResultsInterface;
use Magento\Framework\Api\SearchResults;


class StoreLocatorSearchResults extends SearchResults implements StoreLocatorSearchResultsInterface
{
}
