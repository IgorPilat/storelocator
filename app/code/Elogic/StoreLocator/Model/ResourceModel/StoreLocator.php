<?php

namespace Elogic\StoreLocator\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class StoreLocator extends AbstractDb
{
    public function _construct()
    {
        parent::_init("store_locator", "store_id");
    }
}
