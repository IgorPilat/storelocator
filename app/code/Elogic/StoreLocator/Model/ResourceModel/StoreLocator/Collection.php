<?php

namespace Elogic\StoreLocator\Model\ResourceModel\StoreLocator;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    protected $_idFieldName = 'store_id';
    protected $_eventPrefix = 'elogic_storelocator';
    protected $_eventObject = 'storelocator';

    protected function _construct()
    {
        parent::_init(
            \Elogic\StoreLocator\Model\StoreLocator::class,
            \Elogic\StoreLocator\Model\ResourceModel\StoreLocator::class
        );
    }
}
