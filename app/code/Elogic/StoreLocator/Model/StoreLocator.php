<?php

namespace Elogic\StoreLocator\Model;

use Elogic\StoreLocator\Api\Data\StoreLocatorInterface;
use Magento\Framework\Model\AbstractModel;

class StoreLocator extends AbstractModel implements StoreLocatorInterface
{
    protected $_eventPrefix = 'elogic_storelocator';
    protected $_eventObject = 'storelocator';

    protected function _construct()
    {
        $this->_init(\Elogic\StoreLocator\Model\ResourceModel\StoreLocator::class);
    }

    public function getId()
    {
        return parent::getData(self::STORE_ID);
    }

    public function getStoreName()
    {
        return parent::getData(self::STORE_NAME);
    }

    public function getDescription()
    {
        return parent::getData(self::DESCRIPTION);
    }

    public function getImage()
    {
        return parent::getData(self::IMAGE);
    }

    public function getCity()
    {
        return parent::getData(self::CITY);
    }

    public function getCountry()
    {
        return parent::getData(self::COUNTRY);
    }

    public function getAddress()
    {
        return parent::getData(self::ADDRESS);
    }

    public function getWorkTime()
    {
        return parent::getData(self::WORK_TIME);
    }

    public function getLongitude()
    {
        return parent::getData(self::LONGITUDE);

    }

    public function getLatitude()
    {
        return parent::getData(self::LATITUDE);
    }

    public function getUrlKey()
    {
        return parent::getData(self::URL_KEY);
    }

    public function setId($id)
    {
        return $this->setData(self::STORE_ID, $id);
    }

    public function setStoreName($name)
    {
        return $this->setData(self::STORE_NAME, $name);
    }

    public function setDescription($description)
    {
        return $this->setData(self::DESCRIPTION, $description);
    }

    public function setImage($image)
    {
        return $this->setData(self::IMAGE, $image);
    }

    public function setCity($city)
    {
        return $this->setData(self::CITY, $city);
    }

    public function setCountry($country)
    {
        return $this->setData(self::COUNTRY, $country);
    }

    public function setAddress($address)
    {
        return $this->setData(self::ADDRESS, $address);
    }

    public function setWorkTime($workTime)
    {
        return $this->setData(self::WORK_TIME, $workTime);
    }

    public function setLongitude($longitude)
    {
        return $this->setData(self::LONGITUDE, $longitude);
    }

    public function setLatitude($latitude)
    {
        return $this->setData(self::LATITUDE, $latitude);
    }

    public function setUrlKey($urlKey)
    {
        return $this->setData(self::URL_KEY, $urlKey);
    }
}
