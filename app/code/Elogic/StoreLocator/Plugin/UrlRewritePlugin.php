<?php

namespace Elogic\StoreLocator\Plugin;

use Magento\UrlRewrite\Model\ResourceModel\UrlRewrite as UrlRewriteResource;
use Magento\UrlRewrite\Model\ResourceModel\UrlRewriteCollection;
use Magento\UrlRewrite\Model\ResourceModel\UrlRewriteCollectionFactory;
use Magento\UrlRewrite\Model\UrlRewriteFactory;
use Elogic\StoreLocator\Api\Data\StoreLocatorInterface;
use Elogic\StoreLocator\Model\StoreLocatorRepository;
use Magento\Store\Model\ResourceModel\Store\Collection as StoreCollection;


class UrlRewritePlugin
{
    const TARGET_PATH = 'storelocator/store/view/store_id/';

    const REQUEST_PATH = 'stores/';

    /**
     * @var UrlRewriteFactory
     */
    private $urlRewriteFactory;

    /**
     * @var UrlRewriteCollection
     */
    private $urlRewriteCollection;

    /**
     * @var UrlRewriteCollectionFactory
     */
    private $urlRewriteCollectionFactory;

    /**
     * @var StoreCollection
     */
    protected $storeCollection;

    /**
     * @var UrlRewriteResource
     */
    protected $urlRewriteResource;

    /**
     * UrlRewritePlugin constructor.
     * @param UrlRewriteFactory $urlRewriteFactory
     * @param UrlRewriteCollection $urlRewriteCollection
     * @param UrlRewriteCollectionFactory $urlRewriteCollectionFactory
     * @param StoreCollection $storeCollection
     * @param  UrlRewriteResource $urlRewriteResource
     */
    public function __construct(
        UrlRewriteFactory $urlRewriteFactory,
        UrlRewriteCollection $urlRewriteCollection,
        UrlRewriteCollectionFactory $urlRewriteCollectionFactory,
        StoreCollection $storeCollection,
        UrlRewriteResource $urlRewriteResource
    )
    {
        $this->urlRewriteFactory = $urlRewriteFactory;
        $this->urlRewriteCollection = $urlRewriteCollection;
        $this->urlRewriteCollectionFactory = $urlRewriteCollectionFactory;
        $this->storeCollection = $storeCollection;
        $this->urlRewriteResource = $urlRewriteResource;
    }

    public function afterSave(StoreLocatorRepository $subject, StoreLocatorInterface $storeLocator)
    {
        $id = $storeLocator->getId();
        $urlKey = $storeLocator->getUrlKey();
        $targetPath = (self::TARGET_PATH . $id);
        $requestPath = (self::REQUEST_PATH . $urlKey . ".html/");
        $collection = $this->urlRewriteCollectionFactory->create();
        $collection->addFieldToFilter("target_path", ["eq" => $targetPath]);
        $collection->getSelect();
        if ($collection->count()) {
            foreach ($collection as $item) {
                if ($item->getRequestPath() != $requestPath) {
                    $requestPathNew = (self::REQUEST_PATH . $urlKey);
                    $item->setRequestPath($requestPathNew);
                    $this->urlRewriteResource->save($item);
                }
            }
        } elseif (!$collection->count() && $id != 0){
            $stores = $this->storeCollection->load();
            foreach ($stores as $storeLocator) {
                $urlRewriteModel = $this->urlRewriteFactory->create();
                $urlRewriteModel->setStoreId($storeLocator->getId());
                $urlRewriteModel->setTargetPath($targetPath);
                $urlRewriteModel->setRequestPath($requestPath);
                $this->urlRewriteResource->save($urlRewriteModel);
            }
        }
        return $storeLocator;
    }
}
