<?php

namespace Elogic\StoreLocator\Plugin;

use Elogic\StoreLocator\Model\StoreLocator;

class StorePlugin
{
    public function afterGetDescription(StoreLocator $subject, $result)
    {
        if ($result == null) {
            return "-";
        } else {
            return $result;
        }
    }

    public function afterGetWorkTime(StoreLocator $subject, $result)
    {
        if (empty($result)) {
            return "-";
        } else {
            return $result;
        }
    }
}
