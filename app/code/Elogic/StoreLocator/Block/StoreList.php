<?php

declare(strict_types=1);

namespace Elogic\StoreLocator\Block;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Template;
use Elogic\StoreLocator\Helper\Data as StoreLocatorHelper;
use Elogic\StoreLocator\Helper\Image as ImageHelper;
use Elogic\StoreLocator\Model\ResourceModel\StoreLocator\CollectionFactory as CollectionFactory;
use Elogic\StoreLocator\Model\StoreLocator;

/**
 * Class StoreList
 * @package Elogic\StoreLocator\Block
 */
class StoreList extends Template
{
    /**
     * @var StoreLocator
     */
    protected $storeLocator;
    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;
    /**
     * @var StoreLocatorHelper
     */
    protected $storeLocatorHelper;
    /**
     * @var ImageHelper
     */
    protected $imageHelper;
    /**
     * @var UrlInterface
     */
    protected $url;

    /**
     * View constructor.
     * @param StoreLocator $storeLocator
     * @param CollectionFactory $collectionFactory
     * @param StoreLocatorHelper $storeLocatorHelper
     * @param ImageHelper $imageHelper
     * @param UrlInterface $url
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        StoreLocator $storeLocator,
        CollectionFactory $collectionFactory,
        StoreLocatorHelper $storeLocatorHelper,
        ImageHelper $imageHelper,
        UrlInterface $url,
        Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->storeLocator = $storeLocator;
        $this->collectionFactory = $collectionFactory;
        $this->storeLocatorHelper = $storeLocatorHelper;
        $this->imageHelper = $imageHelper;
        $this->url = $url;
    }

    public function getStores()
    {
        $collection = $this->collectionFactory->create();
        $collection->setOrder("store_name", "asc");
        return $collection;
    }

    public function getStoreImageUrl($store)
    {
        return $this->imageHelper->getImageUrl($store->getImage());
    }
    /**
     * @param $store_id
     * @return string
     */
    public function getStoreUrl($urlKey)
    {
        return $this->url->getUrl('stores/' . $urlKey . '.html');
    }
}
